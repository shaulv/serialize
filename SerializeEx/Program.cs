﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerializeEx
{
    class Program
    {
        static void Main(string[] args)
        {

            Car volvo = new Car()
            {
                Model = "model 3dkdk",
                Brand = "volvo",
                Color = "black",
                Year = 2000
            };

            Car.SerializeACar("car.xml", volvo);

            Car lala = new Car()
            {
                Model = "model 3dffff",
                Brand = "vsdfsdfo",
                Color = "green",
                Year = 2033
            };

            Car[] cars = new Car[]
            {
                volvo,
                lala
            };

            Car.SerializeACarArray("cars.xml", cars);
        }
    }
}
