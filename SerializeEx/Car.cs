﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace SerializeEx
{
    public class Car
    {
        #region Properties
        public string Model { get; set; }
        public string Brand { get; set; }
        public int Year { get; set; }
        public string Color { get; set; }
        int codan;
        protected int numberOfSeats;
        #endregion

        #region Constructors
        public Car()
        {

        }

        public Car(string fileName)
        {
            Car car = Car.DeserializeACar(fileName);

            this.Model = car.Model;
            this.Brand = car.Brand;
            this.Year = car.Year;
            this.Color = car.Color;
        }
        public Car(string model, string brand, int year, string color, int codan, int numberOfSeats)
        {
            this.Model = model;
            this.Brand = brand;
            this.Year = year;
            this.Color = color;

            this.codan = codan;
            this.numberOfSeats = numberOfSeats;
        }
        #endregion

        #region Methods
        public int GetCodan()
        {
            return codan;
        }

        public int GetNumberOfSeats()
        {
            return this.numberOfSeats;
        }

        protected void ChangeNumberOfSeats(int num)
        {
            if (num > 0)
            {
                this.numberOfSeats = num;
            }
        }

        public static void SerializeACar(string filename, Car car)
        {
            XmlSerializer serializer = new XmlSerializer(car.GetType());
            using (Stream file = new FileStream(filename, FileMode.Create))
            {
                serializer.Serialize(file, car);
            }
        }

        public static void SerializeACarArray(string filename, Car[] cars)
        {

            XmlSerializer serializer = new XmlSerializer(cars.GetType());
            using (Stream file = new FileStream(filename, FileMode.Create))
            {
                serializer.Serialize(file, cars);
            }

        }

        public static Car DeserializeACar(string filename)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car));

            Car newCar = null;

            using (Stream file = new FileStream(filename, FileMode.Open))
            {
                newCar = (Car)myXmlSerializer.Deserialize(file);

            } 
            return newCar;
        }

        public static Car[] DeserializeACarArray(string filename)
        {
            XmlSerializer myXmlSerializer = new XmlSerializer(typeof(Car[]));

            Car[] newCars = null;

            using (Stream file = new FileStream(filename, FileMode.Open))
            {
                newCars = (Car[])myXmlSerializer.Deserialize(file);

            }
            return newCars;
        }

        public bool CarCompare(string filename)
        {
            Car car = Car.DeserializeACar(filename);
            if (this.Model == car.Model &&
                this.Brand == car.Brand &&
                this.Year == car.Year &&
                this.Color == car.Color)
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return $"Model:{this.Model}\nBrand:{this.Brand}\nYear: {this.Year}\nColor: {this.Color}\n";
        }
        #endregion
    }
}
